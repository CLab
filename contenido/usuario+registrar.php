<?php
function CONTENIDO_usuario_registrar(){
global $form;
if (isset($_SESSION['regsuccess']) && isset($_SESSION['reguname'])); {
	if($_SESSION['regsuccess'] == true) {
		echo '<h2>Registro de Instructores - Procesando la adición al sistema de '.$_SESSION['reguname'].'</h2><hr />';
		if($_SESSION['regsuccess']){
			echo "<h3>Registrado.</h3><hr />";
			echo "<p><b>'".$_SESSION['reguname']."'</b> ha sido agregado a la base de datos.</p>";
		}else{
			echo "<h3>Registro fallido</h3><hr />";
			echo "<p>Lo sentimos pero el registro para el usuario <b>".$_SESSION['reguname']."</b> a fallado.</p>";
		}
		echo '<hr />';
	}
}
unset($_SESSION['reguname']);
unset($_SESSION['regsuccess']);
?>
<h2>Registros de Instructores</h2><hr />
<?php
if($form->num_errors > 0){
   echo $form->num_errors ." error(es) encontrados";
   echo '<pre>' . print_r($form->getErrorArray(), true) . '</pre>';
}
?>
<form action="include/process.php" method="POST">
<table border="0">
<tr><td>Carné:</td><td><input type="text" name="user" maxlength="30" value="<? echo $form->value("user"); ?>"></td></tr>
<tr><td>Clave:</td><td><input type="password" name="pass" maxlength="30" value="<? echo $form->value("pass"); ?>"></tr>
<tr><td>Email:</td><td><input type="text" name="email" maxlength="50" value="<? echo $form->value("email"); ?>"></td></tr>
<tr><td>Nombre:</td><td><input type="text" name="nombre" maxlength="100" value="<? echo $form->value("nombre"); ?>"></td></tr>
<tr><td>Encargado:</td><td><input type="text" name="encargado" maxlength="100" value="<? echo $form->value("encargado"); ?>"></td></tr>
<tr><td>Catedrático:</td><td><input type="text" name="catedratico" maxlength="100" value="<? echo $form->value("catedratico"); ?>"></td></tr>
<tr><td>Tipo:</td>
<td>
<select name="tipo">
<option value="0" <? if ($form->value("tipo") == 0) {echo 'selected="selected"';}; ?>><? echo TI_0;?>
<option value="1" <? if ($form->value("tipo") == 1) {echo 'selected="selected"';}; ?>><? echo TI_1;?>
<option value="2" <? if ($form->value("tipo") == 2) {echo 'selected="selected"';}; ?>><? echo TI_2;?>
<option value="3" <? if ($form->value("tipo") == 3) {echo 'selected="selected"';}; ?>><? echo TI_3;?>
</select>
</td></tr>
<tr><td>Carrera:</td>
<td>
<select name="departamento">
<option value="0" <? if ($form->value("departamento") == 0) {echo 'selected="selected"';}; ?>><? echo DE_0;?>
<option value="1" <? if ($form->value("departamento") == 1) {echo 'selected="selected"';}; ?>><? echo DE_1;?>
</select>
</td></tr>
<tr><td colspan="2" align="right">
<input type="hidden" name="subjoin" value="1">
<input type="submit" value="Registrar"></td></tr>
</table>
</form>
<?php
}
?>