<?

function CONTENIDO_admin() {
global $session, $database, $form;

function displayUsers(){
   global $database;
   $q = "SELECT username,nombre,userlevel,email,timestamp,tipo,departamento FROM ".TBL_USERS." ORDER BY userlevel DESC;";
   $result = $database->query($q);
   /* Error occurred, return given name by default */
   $num_rows = mysql_numrows($result);
   if(!$result || ($num_rows < 0)){
      echo "Error mostrando la información";
      return;
   }
   if($num_rows == 0){
   /*Esto nunca deberia de pasar realmente...*/
      echo "¡No hay instructores ingresados!";
      return;
   }
   /* Display table contents */
   echo '<table border="1" cellspacing="0" cellpadding="3">';
   echo "<tr><th>Código</th><th>Nombre</th><th>Nivel</th><th>Email</th><th>Última actividad</th><th>Acciones</th></tr>";
   for($i=0; $i<$num_rows; $i++){
      $uname  = mysql_result($result,$i,"username");
      $nombre = mysql_result($result,$i,"nombre");
      $ulevel = mysql_result($result,$i,"userlevel");
      $email  = mysql_result($result,$i,"email");
      $time   = date("d-m-y\nh:ia", mysql_result($result,$i,"timestamp"));
      $verHorario = CREAR_LINK_GET("horarios&amp;user=$uname", "Horario", "Le mostrará la página de horarios adecuada para este Instructor") ;
      //$horarios ='<a href="./?x=hr&amp;forzar='.$uname.'">Ver</a>';
      $reporte = CREAR_LINK_GET("reportes&amp;user=$uname", "Reporte", "Le generará un reporte de el horario.") ;
      $uname = '<a href=./?'._ACC_.'=usuario+info&amp;usr='.$uname.'>'.$uname.'</a>';
      echo "<tr><td>$uname</td><td>$nombre</td><td>$ulevel</td><td>$email</td><td>$time</td><td>$verHorario<hr />$reporte</td></tr>";
   }
   echo "</table><br>\n";
}

echo '<h2>Centro de Administración</h2><hr />';
if($form->num_errors > 0){
   echo "<font size=\"4\" color=\"#ff0000\">"
       ."!*** Error con petición, por favor corregir</font><br><br>";
}
echo '<h3>Instructores registrados en el sistema:</h3>';
displayUsers();

echo '<h3>Establecer permisos a instructor</h3>';
echo $form->error("upduser"); 
?>
<form action="include/adminprocess.php" method="POST">
<table>
<tr>
<td>Código:
<input type="text" name="upduser" maxlength="30" value="<? echo $form->value("upduser"); ?>"></td>
<td>
Nivel:
<select name="updlevel">
<option value="1">Instructor
<option value="9">Administrador
</select>
</td>
<td>
<br>
<input type="hidden" name="subupdlevel" value="1">
<input type="submit" value="Actualizar">
</td></tr>
</table>
</form>
<h3>Quitar Instructor</h3>
<? echo $form->error("deluser"); ?>
<form action="contenido/adminprocess.php" method="POST">
<table>
<td>Código:
<input type="text" name="deluser"  maxlength="30" value="<? echo $form->value("deluser"); ?>"></td>
<input type="hidden" name="subdeluser" value="1">
<td><input type="submit" value="Quitar Instructor"></td>
</table>
</form>
<?php
}
?>