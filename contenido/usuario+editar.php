<?
function CONTENIDO_usuario_editar() {
global $database, $session, $form;
if(!$session->isAdmin()){
   echo "<h3>Acceso denegado</h3>";
   return;
}
/* Si esta en proceso de edición */
if(isset($_SESSION['useredit'])){
   unset($_SESSION['useredit']);
   
   echo "<h2>¡Cuenta de usuario editada exitosamente!</h2><hr />";
   echo "<h3>La cuenta de ".$_SESSION['user_edic'] ." ha sido exitosamente actualizada.</h3>";
   return;
}
$_SESSION['user_edic'] = $_GET['usr'];
$req_user_info = $database->getUserInfo($_SESSION['user_edic']);
?>
<h2>Editar cuenta del Instructor: <? echo $_SESSION['user_edic']; ?></h2>
<form action="include/process.php" method="POST">
<table  border="0" cellspacing="0">
<tr>
<td>Nueva clave:</td>
<td><input type="password" name="newpass" maxlength="30" value=""></td>
</tr>

<tr>
<td>Email:</td>
<td><input type="text" name="email" maxlength="50" value="<? echo $req_user_info['email']; ?>"></td>
</tr>

<tr>
<td>Nombre:</td>
<td><input type="text" name="nombre" maxlength="100" value="<? echo $req_user_info['nombre']; ?>"></td>
</tr>

<tr>
<td>Encargado:</td>
<td><input type="text" name="encargado" maxlength="100" value="<? echo $req_user_info['encargado']; ?>"></td>
</tr>

<tr>
<td>Catedrático:</td>
<td><input type="text" name="catedratico" maxlength="100" value="<? echo $req_user_info['catedratico']; ?>"></td>
</tr>

<tr>
<td>Tipo:</td>
<td>
<select name="tipo">
<option value="0" <? if ($req_user_info['tipo'] == 0) {echo 'selected="selected"';}; ?>>Asistente de Catedrático
<option value="1" <? if ($req_user_info['tipo'] == 1) {echo 'selected="selected"';}; ?>>Asistente de Taller
<option value="2" <? if ($req_user_info['tipo'] == 2) {echo 'selected="selected"';}; ?>>Encargado de Taller
<option value="3" <? if ($req_user_info['tipo'] == 3) {echo 'selected="selected"';}; ?>>Instructor de Materia
</select>
</td>
</tr>

<tr>
<td>Departamento:</td>
<td>
<select name="departamento">
<option value="0" <? if ($req_user_info['departamento'] == 0) {echo 'selected="selected"';}; ?>>Dpto. Electrónica
<option value="1" <? if ($req_user_info['departamento'] == 1) {echo 'selected="selected"';}; ?>>Dpto. Informática
</select>
</td>
</tr>

<tr>
<td></td>
<td><input type="submit" value="Editar cuenta"></td>
</tr>
</table>
<input type="hidden" name="subedit" value="1">
<input type="hidden" name="username" value="<?echo $_SESSION['user_edic']; ?>">
</form>

<?
}
?>