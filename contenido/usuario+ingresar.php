<?php
function CONTENIDO_usuario_ingresar() {

	global $session, $form;
	echo '<h2>Ingreso al Sistema de Horarios</h3><hr>';
	
	/* Ya se encuentra registrado */
	if($session->logged_in){
		echo "<h3>¡Bienvenido!</h3><hr><br />";
		echo "<b>$session->username</b>, Ud. ha ingresado al sistema.<br />";
		if($session->isAdmin()){
		echo 'Puede encontrar sus herramientas administrativas en los menús laterales<br />';
		}
		echo '<hr />';
		echo CREAR_LINK_GET("usuario+info&amp;usr=$session->username", "Continuar a Mi Perfil" , "Avanzar hacia su perfil");
		/* Limpiamos todo lo que podamos */
		unset($_SESSION['reguname']);
		unset($_SESSION['regsuccess']);
		return;
	
	}
	
	/* Fallo en el registro */
	if(isset($_SESSION['regsuccess']) && $_SESSION['regsuccess'] == false){
	echo "<h3>Error - Por favor intentelo de nuevo.</h3><hr>";
	print_r($_SESSION['error_array']);
	}
	/* Empezar en limpio */
	unset($_SESSION['regsuccess']);

?>
<form action="include/process.php" method="post">
<table border=0>
<tr>
<td>Usuario o Carné:</td>
<td><input type="text" name="user" maxlength="10" size="30" value="" /></td>
</tr>
<tr>
<td>Clave de acceso:</td>
<td><input type="password" name="pass" maxlength="20" size="30" value="" /></td>
</tr>
<tr>
<td>Recordarme:</td>
<td><input type="checkbox" name="remember"'; <? if($form->value("remember")){ echo "checked"; } ?>
' /></td>
</tr>
</table>
<input type="hidden" name="sublogin" value="1">
<center><input type="submit" name="ingresar" value="Ingresar" /></center>
</form>
<br /><? echo CREAR_LINK_GET("rpr+clave", "Recuperar clave", "Clic en este enlace para intentar recuperar su clave"); ?></a>
</ul>
<?
}
?>