<?php
function byteConvert($bytes)
{
	$s = array('B', 'Kb', 'MB', 'GB', 'TB', 'PB');
	$e = floor(log($bytes)/log(1024));

	return sprintf('%.2f '.$s[$e], ($bytes/pow(1024, floor($e))));
}
function Directorios($base, $tema) {
	$dir=dir($base);
	$s = "<h4>$tema</h4><blockquote cite=\"$tema\"><ol>";
	while($filename=$dir->read()) {
		if ( $filename != "." && $filename != ".." && $filename != "index.php" ) { $s = $s."<li>".CREAR_LINK("$base$filename", $filename, "Abrir $filename").' [<b>'.byteConvert(filesize($base.$filename)).'</b>]</li><br />'; }
	}
	$dir->close();
	$s = $s."</ol></blockquote>";
	return $s;
}

$HTML_HEAD = '
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Reporte de Instructores</title>
	</head>
	<body>
	';
$HTML_FOOT = '</body></html>';

function HORARIO__DESCRIBIR_ELEMENTO_EN_POS_UNICO_ECHO($dia, $hora) {
	global $session, $link;
	// 1. Tengo ocupada esa hora?
	switch ( $_SESSION['tipo'] ) {
	case 0:
	case 1:
	case 3:
	case 2:
	$q = "SELECT users.username, nombre, horarios.taller FROM users,horarios WHERE horarios.username =  '".$_SESSION['user'] . "' AND users.username =  '".$_SESSION['user']."' AND tipo = '".$_SESSION['tipo']."' AND dia='$dia' AND posicion='$hora'";
	break;
	}
	//echo $q;
	$resultados = @mysql_query($q, $link);
	$num_rows = mysql_numrows($resultados);

	 if($num_rows > 0){
		//Los encargados estan ocupados y punto.
		if ( $_SESSION['tipo'] == 2 ) {
			$msj = $msj.'<FONT COLOR="#800000">'."Ocupado"."</font><br />";
		} else {
			//Si, la tengo ocupada, ¿en que taller estoy?
			//Hacemos for para mostrar si hay mas de un taller (error)!
			for($i=0; $i<$num_rows; $i++){
				$msj = $msj.'<FONT COLOR="#800000">'.mysql_result($resultados,$i,"taller")."</font><br />";
			}
		}
	}else {
			$msj="";	
	}
return "<td>".$msj."</td>";
}

function MOSTRAR_HORARIOS_UNICO_ECHO() {
	/* Muestra  la tabla de horario en base a las opciones de filtro establecidas */
	global $link, $base, $motor, $usuario, $clave;
	$link = @mysql_connect($motor, $usuario, $clave) or die('Por favor revise sus datos, puesto que se produjo el siguiente error:<br /><pre>' . mysql_error() . '</pre>');
	@mysql_select_db($base, $link) or die('!->La base de datos seleccionada "'.$base.'" no existe');
	$s = "";
	$s = $s . "<h3>";
	switch ($_SESSION['dpto'])
	{
	case 0: $s = $s . DE_0; break;
	case 1: $s = $s . DE_1; break;
	}
	$s = $s . 
	'  - Instructor: '.$_SESSION['nombre'].'</h3>
	<hr />
	<table border="1" cellspacing="0" cellpadding="3">
	<tr><th>Horario</th><th>Lunes</th><th>Martes</th><th>Miercoles</th><th>Jueves</th><th>Viernes</th><th>Sabado</th></tr>
	';
	for ($i=450; $i<=1170; $i+=60){
	$s = $s .  "<tr><td><b>". date("h:ia", mktime(0,$i)) . "</b></td>". HORARIO__DESCRIBIR_ELEMENTO_EN_POS_UNICO_ECHO(1, $i).HORARIO__DESCRIBIR_ELEMENTO_EN_POS_UNICO_ECHO(2, $i).HORARIO__DESCRIBIR_ELEMENTO_EN_POS_UNICO_ECHO(3, $i).HORARIO__DESCRIBIR_ELEMENTO_EN_POS_UNICO_ECHO(4, $i).HORARIO__DESCRIBIR_ELEMENTO_EN_POS_UNICO_ECHO(5, $i).HORARIO__DESCRIBIR_ELEMENTO_EN_POS_UNICO_ECHO(6, $i)."</tr>";
	}
	$s = $s .  "</table>";
	mysql_close($link);
	return $s;
}

function displayUsers_ECHO(){
  global $database, $link, $base, $motor, $usuario, $clave;
  $link = @mysql_connect($motor, $usuario, $clave) or die('Por favor revise sus datos, puesto que se produjo el siguiente error:<br /><pre>' . mysql_error() . '</pre>');
  mysql_select_db($base, $link) or die('!->La base de datos seleccionada "'.$base.'" no existe');   
  //¿Algún WHERE?
   $where = "";
   $q = "SELECT username,nombre,userlevel,email,timestamp FROM ".TBL_USERS." $where ORDER BY userlevel DESC;";
   $result = $database->query($q, $link);
   /* Error occurred, return given name by default */
   $num_rows = mysql_numrows($result);
   if(!$result || ($num_rows < 0)){
      echo "<hr />Error en la consulta<hr />";
      return;
   }
   if($num_rows == 0){
      $s = "No hay instructores, imposible generar reporte.";
      return;
   }
   /* Display table contents */
   $s = "";
   $s = '<table border="1" cellspacing="0" cellpadding="3">';
   $s = $s . "<tr><td><b>Código</b></td><td><b>Nombre</b></td><td><b>Nivel</b></td><td><b>Email</b></td><td><b>Última actividad</b></td></tr>";
   for($i=0; $i<$num_rows; $i++){
      $uname  = mysql_result($result,$i,"username");
      $nombre = mysql_result($result,$i,"nombre");
      $ulevel = mysql_result($result,$i,"userlevel");
      $email  = mysql_result($result,$i,"email");
      $time   = date("c", mysql_result($result,$i,"timestamp"));
      //$horarios ='<a href="./?x=hr&amp;forzar='.$uname.'">Ver</a>';
      $uname = $uname;
      $s = $s . "<tr><td>$uname</td><td>$nombre</td><td>$ulevel</td><td>$email</td><td>$time</td></tr>";
   }
   $s = $s . "</table>";
   return $s;
}

function OBTENER_TIPO_R($taller,$dia, $hora) {
	global $session, $link;
	// ¿Hay alguien(es) en esa posición?
	switch ($_SESSION['tipo']) {
	case 0:
	case 1:
	case 3:
		$q = "SELECT users.username, nombre FROM users, horarios WHERE users.username = horarios.username and taller like '%$taller%' AND tipo = '". $_SESSION['tipo']. "' AND dia='$dia' AND posicion='$hora' AND departamento = '" .$_SESSION['dpto']. "';";
		break;
	case 2:
		$q = "SELECT users.username, nombre FROM users, horarios WHERE users.username = horarios.username AND tipo = '2' AND dia='$dia' AND posicion='$hora' AND departamento = '" . $_SESSION['dpto']. "';";
		break;
	}
	//echo $q;
	$resultados = @mysql_query($q, $link);
	$num_rows = mysql_numrows($resultados);

	 if($num_rows > 0){
	 $msj="";
		   for($i=0; $i<$num_rows; $i++){
		      $uname = mysql_result($resultados,$i,"nombre");
		      /* Por petición solo el primer nombre es mostrado */
		      unset ($nombre);
		      ereg("([^ ]*)", $uname, $nombre);
		      $uinfo = mysql_result($resultados,$i,"username");
		      $msj = $msj.$nombre[0]."<br />";
		   }
		return "<td bgcolor=\"#EEEEEE\">".$msj."</td>";
	}else {
		//$msj="Sin Asignar";
		$msj="";
	}
	return "<td>".$msj."</td>";
}

function MOSTRAR_HORARIOS_ECHO() {
global $link, $base, $motor, $usuario, $clave; 
$link = @mysql_connect($motor, $usuario, $clave) or die('Por favor revise sus datos, puesto que se produjo el siguiente error:<br /><pre>' . mysql_error() . '</pre>');
mysql_select_db($base, $link) or die('!->La base de datos seleccionada "'.$base.'" no existe');
$salida =
'
<hr />
<h3>Taller '.$_SESSION['taller'].' - ';
switch ($_SESSION['tipo']) 
{
case 0: $salida = $salida .TI_0; break;
case 1: $salida = $salida .TI_1; break;
case 2: $salida = $salida .TI_2; break;
case 3: $salida = $salida .TI_3; break;
}

$salida = $salida .
'</h3>
<hr />
<table border="1"  cellspacing="0" cellpadding="3">
<tr><th>Horario</th><th>Lunes</th><th>Martes</th><th>Miercoles</th><th>Jueves</th><th>Viernes</th><th>Sabado</th></tr>
';
for ($i=450; $i<=1170; $i+=60){
$salida = $salida ."<tr><td>". date("h:ia", mktime(0,$i)) . "</td>". OBTENER_TIPO_R($_SESSION['taller'], 1, $i).OBTENER_TIPO_R($_SESSION['taller'], 2, $i).OBTENER_TIPO_R($_SESSION['taller'], 3, $i).OBTENER_TIPO_R($_SESSION['taller'], 4, $i).OBTENER_TIPO_R($_SESSION['taller'], 5, $i).OBTENER_TIPO_R($_SESSION['taller'], 6, $i)."</tr>";
}
$salida = $salida .'</table><br /><p style="page-break-after: always" />';
//mysql_close($link);
return $salida;
}

function ADMIN_reportes() {
	global $HTML_HEAD, $HTML_FOOT, $database;;
	echo "<h2>Generador de Reportes</h2>";
	/***************************************************************************************************************************/
	/*							USER      													        	*/
	/***************************************************************************************************************************/
	//Nos pasaron un nombre?, entonces quieren un reporte SOLO de ese usuario:
	if ( isset($_GET['user']) ) {
		$_SESSION['user'] = $_GET['user'];
		$req_user_info = $database->getUserInfo($_SESSION['user']);
		$_SESSION['tipo'] = $req_user_info['tipo'];
		$_SESSION['nombre'] = $req_user_info['nombre'];
		$_SESSION['dpto'] = $req_user_info['departamento'];
		unset($_SESSION['taller']);
		/***************************************************************************************************************************/
		/*							USER - HTML													        */
		/***************************************************************************************************************************/
		$s = $HTML_HEAD. MOSTRAR_HORARIOS_UNICO_ECHO() . $HTML_FOOT;
		$archivo_HTML_INSTRUCTORES_IND = "reportes/+I/+HTML/instructores+".$_SESSION['user']."+".$tiempo_ord.".html";
		$fh = @fopen($archivo_HTML_INSTRUCTORES_IND, 'w') or die("'/reportes/+HTML/' bloqueado");
		fwrite($fh, $s);
		fclose($fh);
		/***************************************************************************************************************************/
		/*							USER - PDF													        */
		/***************************************************************************************************************************/
		@set_time_limit(300);
		$dompdf = new DOMPDF();
		$dompdf->load_html($s);
		$dompdf->render();
		$PDF_INSTRUCTORES_IND = $dompdf->output();
		$archivo_PDF_INSTRUCTORES_IND = "reportes/+I/+PDF/horarios+".$_SESSION['user']."+".$tiempo_ord.".pdf";
		file_put_contents($archivo_PDF_INSTRUCTORES_IND, $PDF_INSTRUCTORES_IND);
		unset($dompdf);
		unset($PDF_INSTRUCTORES);
		@set_time_limit(30);
		
		/* LINKS */
		echo "<pre>";
		echo '<a href="'.$archivo_HTML_INSTRUCTORES_IND.'" target="_blank">Descargar reportes de horario para '.$_SESSION['user'].'[HTML]</a><br />'; 
		echo '<a href="'.$archivo_PDF_INSTRUCTORES_IND.'" target="_blank">Descargar reportes de horario para '.$_SESSION['user'].'[PDF]</a><br />'; 
		echo "</pre>";
		echo '<blockquote>Por favor realice clic derecho sobre el enlace de descarga y posteriormente utilice la opción "Guardar como" de su navegador</blockquote>';
		/* FIN USER */
		return;
	}
	if ( isset($_POST['generar']) ) {
		echo '<h3>Reporte(s) generado(s) en base a las opciones de configuración:<br />';
		$tiempo_ord = date('y\-m\-d\+h.ia', time());
		if ($_POST['generar_horarios'] == 1 ) {
		/***************************************************************************************************************************/
		/*							HORARIOS - HTML												        */
		/***************************************************************************************************************************/
		$s = $HTML_HEAD;
		if (isset($_POST['dpto0'])) { 
		if ($_POST['dpto0'] == 1) {
			$_SESSION['dpto'] = 0;
			$s = $s . "<h2>".DE_0. "</h2>";
			for ($i=0; $i < 4; $i++) {
			if (isset($_POST["tipo_instructor$i"])) { 
				if ( $_POST["tipo_instructor$i"] == 1 ) {
					$_SESSION['tipo'] = $i;
					if ($i != 2 ) {
					if (isset($_POST['taller0'])) { if ( $_POST['taller0'] ) {$_SESSION['taller'] = 'LSA'; $s = $s . MOSTRAR_HORARIOS_ECHO(); }}
					if (isset($_POST['taller1'])) { if ( $_POST['taller1'] ) {$_SESSION['taller'] = 'LID'; $s = $s . MOSTRAR_HORARIOS_ECHO(); }}
					if (isset($_POST['taller2'])) { if ( $_POST['taller2'] ) {$_SESSION['taller'] = 'LAI'; $s = $s . MOSTRAR_HORARIOS_ECHO(); }}
					if (isset($_POST['taller3'])) { if ( $_POST['taller3'] ) {$_SESSION['taller'] = 'LIV'; $s = $s . MOSTRAR_HORARIOS_ECHO(); }}
					} else {
					//Encargado de Taller, solo mostrar 1 horario.
					$_SESSION['taller'] = 'GENERAL'; $s = $s . MOSTRAR_HORARIOS_ECHO(); 
					}
				}
			}
			}
		}
		}
		if (isset($_POST['dpto1'])) { 
		if ($_POST['dpto1'] == 1) {
			$_SESSION['dpto'] = 1;
			$s = $s . "<h2>".DE_1. "</h2>";
			for ($i=0; $i < 4; $i++) {
			if (isset($_POST["tipo_instructor$i"])) { 
				if ( $_POST["tipo_instructor$i"] == 1 ) {
					$_SESSION['tipo'] = $i;
					if ($i != 2 ) {
					if (isset($_POST['taller4'])) { if ( $_POST['taller4'] ) {$_SESSION['taller'] = 'TPP'; $s = $s . MOSTRAR_HORARIOS_ECHO(); }}
					if (isset($_POST['taller5'])) { if ( $_POST['taller5'] ) {$_SESSION['taller'] = 'TIS'; $s = $s . MOSTRAR_HORARIOS_ECHO(); }}
					if (isset($_POST['taller6'])) { if ( $_POST['taller6'] ) {$_SESSION['taller'] = 'TAS'; $s = $s . MOSTRAR_HORARIOS_ECHO(); }}
					if (isset($_POST['taller7'])) { if ( $_POST['taller7'] ) {$_SESSION['taller'] = 'TEC'; $s = $s . MOSTRAR_HORARIOS_ECHO(); }}
					} else {
					//Encargado de Taller, solo mostrar 1 horario.
					$_SESSION['taller'] = 'GENERAL'; $s = $s . MOSTRAR_HORARIOS_ECHO(); 
					}
				}
			}
			}
		}
		}
		$s = $s . $HTML_FOOT;

		//Haremos HORARIOS_HTML?
		if ( $_POST['tipo_reporte1'] == 1 ) {
		$archivo_HTML_HORARIOS = "reportes/+H/+HTML/horarios+".$tiempo_ord.".html";
		$fh = @fopen($archivo_HTML_HORARIOS, 'w') or die("'/reportes/+HTML/' bloqueado");
		fwrite($fh, $s);
		fclose($fh);
		}
		
		/***************************************************************************************************************************/
		/*							HORARIOS - PDF												        */
		/***************************************************************************************************************************/	
		
		if ( $_POST['tipo_reporte0'] == 1 ) {
		@set_time_limit(300);
		$dompdf = new DOMPDF();
		$dompdf->load_html($s);
		$dompdf->render();
		$PDF_HORARIOS = $dompdf->output();
		$archivo_PDF_HORARIOS = "reportes/+H/+PDF/horarios+".$tiempo_ord.".pdf";
		file_put_contents($archivo_PDF_HORARIOS, $PDF_HORARIOS);
		unset($dompdf);
		unset($PDF_HORARIOS);
		@set_time_limit(30);
		}
		}
		
		/***************************************************************************************************************************/
		/*							INSTRUCTORES - HTML											        */
		/***************************************************************************************************************************/
		if ( $_POST['generar_instructores'] == 1 ) {
		$s = $HTML_HEAD. displayUsers_ECHO() . $HTML_FOOT;
		
		//Haremos Instructores_HTML?
		if ( $_POST['tipo_reporte1'] == 1 ) {
		$archivo_HTML_INSTRUCTORES = "reportes/+I/+HTML/instructores+".$tiempo_ord.".html";
		$fh = @fopen($archivo_HTML_INSTRUCTORES, 'w') or die("'/reportes/+HTML/' bloqueado");
		fwrite($fh, $s);
		fclose($fh);
		}
		
		/***************************************************************************************************************************/
		/*							INSTRUCTORES - PDF												*/
		/***************************************************************************************************************************/
		if ( $_POST['tipo_reporte0'] == 1 ) {
		@set_time_limit(300);
		$dompdf = new DOMPDF();
		$dompdf->load_html($s);
		$dompdf->render();
		$PDF_INSTRUCTORES = $dompdf->output();
		$archivo_PDF_INSTRUCTORES = "reportes/+I/+PDF/horarios+".$tiempo_ord.".pdf";
		file_put_contents($archivo_PDF_INSTRUCTORES, $PDF_INSTRUCTORES);
		unset($dompdf);
		unset($PDF_INSTRUCTORES);
		@set_time_limit(30);
		}
		}
		
		/***************************************************************************************************************************/
		/* 			GENERACIÓN DE REPORTES TERMINANDA, MOSTRAR LINKS */
		/***************************************************************************************************************************/
		echo "<pre>";
		if ( $_POST['tipo_reporte1'] == 1 && $_POST['generar_horarios'] == 1 ){ echo '<a href="'.$archivo_HTML_HORARIOS.'" target="_blank">Descargar reportes de Horarios[HTML]</a><br />'; }
		if ( $_POST['tipo_reporte0'] == 1 && $_POST['generar_horarios'] == 1 ) {echo '<a href="'.$archivo_PDF_HORARIOS.'" target="_blank">Descargar reportes de Horarios[PDF]</a><br />'; }
		if ( $_POST['tipo_reporte1'] == 1 && $_POST['generar_instructores'] == 1 ){ echo '<a href="'.$archivo_HTML_INSTRUCTORES.'" target="_blank">Descargar reportes de Instructores[HTML]</a><br />'; }
		if ( $_POST['tipo_reporte0'] == 1 && $_POST['generar_instructores'] == 1 ){ echo '<a href="'.$archivo_PDF_INSTRUCTORES.'" target="_blank">Descargar reportes de Instructores[PDF]</a><br />'; }
		echo "</pre>";
		echo '<blockquote>Por favor realice clic derecho sobre el enlace de descarga y posteriormente utilice la opción "Guardar como" de su navegador</blockquote>';
	}
	echo "<hr /><h3>Por favor seleccione el/los tipo(s) de reporte(s) a generar</h3>";
	echo "<h4>Reporte de horarios</h4>";
	?>
	<form action="./?accion=reportes" method="post">
	<input type="checkbox" name="generar_horarios" value="1" checked="cheked">Generar reporte de horarios
	<table>
	<tr>
	<td><input type="checkbox" name="dpto0" value="1" checked="cheked"><?echo DE_0;?></td>
	<td><input type="checkbox" name="dpto1" value="1" checked="cheked"><?echo DE_1;?></td>
	</tr>
	</table>
	<table>
	<tr>
	<td><input type="checkbox" name="taller0" value="1" checked="cheked">LSA</td>
	<td><input type="checkbox" name="taller4" value="1" checked="cheked">TPP</td>
	</tr>
	<tr>
	<td><input type="checkbox" name="taller1" value="1" checked="cheked">LID</td>
	<td><input type="checkbox" name="taller5" value="1" checked="cheked">TIS</td>
	</tr>
	<tr>
	<td><input type="checkbox" name="taller2" value="1" checked="cheked">LAI</td>
	<td><input type="checkbox" name="taller6" value="1" checked="cheked">TAS</td>
	</tr>
	<tr>
	<td><input type="checkbox" name="taller3" value="1" checked="cheked">LIV</td>
	<td><input type="checkbox" name="taller7" value="1" checked="cheked">TEC</td>
	</tr>
	</table>
	<table>
	<tr>
	<td><input type="checkbox" name="tipo_instructor0" value="1" checked="cheked"> <?echo TI_0;?></td>
	<td><input type="checkbox" name="tipo_instructor1" value="1" checked="cheked"> <?echo TI_1;?></td>
	</tr>
	<tr>
	<td><input type="checkbox" name="tipo_instructor2" value="1" checked="cheked"> <?echo TI_2;?></td>
	<td><input type="checkbox" name="tipo_instructor3" value="1" checked="cheked"> <?echo TI_3;?></td>
	</tr>
	</table>
	<? echo "<h4>Reporte de Instructores (listas)</h4>"; ?>
	<input type="checkbox" name="generar_instructores" value="1" checked="cheked">Generar reporte de instructores
	<table>
	<tr>
	<td><input type="checkbox" name="instructor_nivel0" value="1" checked="cheked"> Administradores</td>
	<td><input type="checkbox" name="instructor_nivel1" value="1" checked="cheked"> Instructores</td>
	</tr>
	</table>
	<? echo "<hr /><h3>Por favor seleccione el/los tipo(s) de formato de salida a generar</h3>"; ?>
	<table>
	<tr>
	<td><input type="checkbox" name="tipo_reporte0" value="1" checked="cheked">PDF</td>
	<td><input type="checkbox" name="tipo_reporte1" value="1" checked="cheked">HTML</td>
	</tr>
	</table>
	<input type="hidden" name="generar" value="1">
	<input type="submit" name="bgenerar" value="Generar" />
	</form>
	<?
	echo "<hr /><h3>Se han encontrado los siguientes reportes anteriormente generados</h3>";
	echo Directorios("reportes/+H/+HTML/", "Horarios - HTML");
	echo Directorios("reportes/+H/+PDF/", "Horarios - PDF");
	echo Directorios("reportes/+I/+HTML/", "Instructores - HTML");
	echo Directorios("reportes/+I/+PDF/", "Instructores - PDF");

	return;
}
?>