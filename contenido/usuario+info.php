<?
function CONTENIDO_usuario_info(){
global $session, $database;
/* Requested Username error checking */
$req_user = trim($_GET['usr']);
if(!$req_user || strlen($req_user) == 0 ||
   !eregi("^([0-9a-z])+$", $req_user) ||
   !$database->usernameTaken($req_user)){
   die("Usuario no registrado");
}

/* Logged in user viewing own account */
if(strcmp($session->username,$req_user) == 0){
   echo "<h2>Mi cuenta</h2><hr />";
}
/* Visitor not viewing own account */
else{
   echo "<h2>Información del usuario</h2><hr />";
}

/* Display requested user information */
$req_user_info = $database->getUserInfo($req_user);
echo '<table>';
echo "<tr><td><b>Código de usuario:</b></td><td>".$req_user_info['username']."</td></tr>";
echo "<tr><td><b>Nombre de usuario:</b></td><td>".$req_user_info['nombre']."</td></tr>";
echo "<tr><td><b>Encargado:</b></td><td>".$req_user_info['encargado']."</td></tr>";
echo "<tr><td><b>Catedrático:</b></td><td>".$req_user_info['catedratico']."</td></tr>";
echo "<tr><td><b>Tipo de Instructor:</b></td><td>";
switch ($req_user_info['tipo']) {
case 0:
	echo TI_0;
	break;
case 1:
	echo TI_1;
	break;
case 2:
	echo TI_2;
	break;
case 3:
	echo TI_3;
	break;
default:
	echo "Tipo de instructor desconocido";
	break;
}
echo"</td></tr>";
echo "<tr><td><b>Departamento:</b></td><td>";
switch ($req_user_info['departamento']) {
case 0:
	echo DE_0;
	break;
case 1:
	echo DE_1;
	break;
}
echo"</td></tr>";
echo "<tr><td><b>Email:</b></td><td>".$req_user_info['email']."</td></tr></table>";

if($session->isAdmin()){
   echo "<hr />".CREAR_LINK_GET("usuario+editar&amp;usr=$req_user", "Editar información de la cuenta", "Modifica los detalles de esta cuenta");
}
}
?>