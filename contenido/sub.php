<?php
/*Crear un link HTML*/
function CREAR_LINK($sAccion, $sTexto, $sTitulo) {
	return "<a href=\"$sAccion\" title=\"$sTitulo\">$sTexto</a>";
}

/*Crear un link apropiado para GET*/
function CREAR_LINK_GET($sAccion, $sTexto, $sTitulo) {
	return "<a href=\"?accion=$sAccion\" title=\"$sTitulo\">$sTexto</a>";
}

function MENU_usuario(){
	global $session;
	echo '
	<dl class="box">
	<dl class="box"><dt><b>Usuario</b></dt></dl>
	<dt>'.$session->username.'</dt>';
	echo '<dd><ul class="menu">';
	if($session->logged_in){
		echo '<li>'.CREAR_LINK("include/process.php","Salir", "Salir del sistema").'</li>';
		echo '<li>'. CREAR_LINK_GET("usuario+info&amp;usr=".$session->username, 'Mi Cuenta', "Ver los datos de su perfil").'</li>';
	} else {
		echo '<li>'.CREAR_LINK_GET("ingresar", "Ingresar", "Si ya esta registrado puede volver a ingresar al sistema con este enlace").'</li>';
	}

	if($session->isAdmin()){
		echo CREAR_LINK_GET("reportes", "Reportes", "Genera reportes de los horarios e instructores");
	}
	echo '</ul></dd></dl>';
}


function MENU_en_linea(){
	global $session, $database;
	echo	'<dl class="box">
	<dt>En línea ('. ($database->num_active_users + $database->num_active_guests) . ')</dt>
	<dd>
	<ul class="menu">
	';
	echo '<li>Usuarios: ' . $database->num_active_users . '</li>';
	echo '<li>Visitantes: ' . $database->num_active_guests . '</li><li><hr /></li>';
	if(!defined('TBL_ACTIVE_USERS')) {
	  die("Error processing page");
	}

	$q = "SELECT username FROM ".TBL_ACTIVE_USERS
	    ." ORDER BY timestamp DESC,username";
	$result = $database->query($q);
	$num_rows = mysql_numrows($result);
	if(!$result || ($num_rows < 0)){
	   echo "Error displaying info";
	}
	else if($num_rows > 0){
	   for($i=0; $i<$num_rows; $i++){
	      $uname = mysql_result($result,$i,"username");
	      echo '<li><a href="./?'._ACC_.'=usuario+info&amp;usr='.$uname.'">'.$uname.'</a></li>';
	   }
	}
	echo
	'
	</ul>
	</dd>
	</dl>
	';
}
function MENU_informacion() {
	echo
	'<dl class="box">
	<dt>Información</dt>
	<dd>
	<ul class="menu">
		<li>'.CREAR_LINK_GET("infosis", "Sistema", "Ver la configuración del servidor actual").'</li>
	</ul>
	</dd>
	</dl>';
}
function MENU_gestion() {
global $session;
	if($session->logged_in){
	echo '<dl class="box"><dt><b>Gestión</b></dt></dl>';
	}
	if($session->isAdmin()){
	echo
	'
	<dl class="box">
	<dt>Instructores</dt>
	<dd>
	<ul class="menu">' .
		'<li>'.CREAR_LINK_GET("registro","Agregar", "Agregar un Instructor al sistema")."</li>" .
		'<li>'.CREAR_LINK_GET("admin","Gestionar", "Eliminar y modificar Instructores")."</li>" .
	'</ul>
	</dd>
	</dl>
	<dl class="box">
	<dt>Horarios</dt>
	<dd>
	<ul class="menu">'.
		'<li>'.CREAR_LINK_GET("horarios","Gestionar", "Eliminar y modificar Horarios")."</li>" .
	'</ul>
	</dd>
	</dl>
	';
	} 
	if($session->logged_in && !$session->isAdmin()){
	echo
	'
	<dl class="box">
	<dt>Horarios</dt>
	<dd>
	<ul class="menu">
		<li><a href="./?'._ACC_.'=horarios">Consultar</a></li>
	</ul>
	</dd>
	</dl>
	';
	}
}

function CONTENIDO_mostrar_principal() {
	global $session;
	/* Verificamos si es permitido  ver el sitio sin estar registrado, si no forzamor a ir al registro*/
	switch ($_SESSION[_ACC_]) {
	case "infosis": break;
	case "ayuda contacto": break;
	case "rpr clave": break;
	default: if (!$session->logged_in||!isset($_SESSION[_ACC_])){$_SESSION[_ACC_] = "ingresar";}
	}

	switch ($_SESSION[_ACC_] ) {

	case "usuario info":
		CONTENIDO_usuario_info();
		break;
	
	case "usuario editar":
		CONTENIDO_usuario_editar();
		break;
	case "rpr clave":
		CONTENIDO_recuperar_clave();
		break;
		
	case "admin":
		if($session->isAdmin()){
			CONTENIDO_admin();
			break;
		}

	case "registro":
		if($session->isAdmin()){
			CONTENIDO_usuario_registrar();
			break;
		}

	case "ingresar":
		CONTENIDO_usuario_ingresar();
		break;

	case "ayuda contacto":
		CONTENIDO_ayuda_contacto() ;
		break;
		
	case "infosis":
		echo phpinfo();
		break;
		
	case "horarios":
		CONTENIDO_horarios();
		break;

	case "reportes":
		ADMIN_reportes();
		break;
		
	default:
		CONTENIDO_global_404();
	}
	if(!$session->isAdmin()){
		echo '<hr />Sugerencias y Consultas: '. CREAR_LINK_GET("ayuda+contacto","contacto con el Administrador", "Útil para obtener ayuda, hacer comentarios y reportar errores al Adminsitrador de este sitio");
	}
}
?>