<?php
/* Activar compresión de salida */
ob_start("ob_gzhandler"); 

/*-----------------------DEFINICIONES-------------------*/
define("_NOMBRE_", "Sistema de Horarios");
define("_ACC_", "accion");
/**/
define("TI_0","ASISTENTE DE CATEDRATICO");
define("TI_1","ASISTENTE DE TALLER");
define("TI_2","ENCARGADO DE TALLER");
define("TI_3","INTRUCTOR DE MATERIA");
define("DE_0","Carrera de Electrónica");
define("DE_1","Carrera de Informática");
/**/
define("CONTINUAR",'<a href="./?'._ACC_.'=horarios">Continuar</a>');

/*-----------------------INCLUSIONES-------------------*/
/* CODIGO */
/* Controlador principal de la sesión */
require_once("include/depurar.php");
require_once("include/sesion.php");
/* Datos de configuración para el servidor */
require_once("include/data.php");
/*Para procesar PDF's*/
require_once ('include/dompdf/dompdf_config.inc.php');
/* CONTENIDO */
/*Constructores de Menús, etc.*/
require_once("contenido/sub.php");
require_once("contenido/admin.php");
require_once("contenido/ayuda+contacto.php");
require_once("contenido/usuario+recuperar_clave.php");
require_once("contenido/usuario+info.php");
require_once("contenido/usuario+horarios.php");
require_once("contenido/usuario+ingresar.php");
require_once("contenido/usuario+registrar.php");
require_once("contenido/usuario+editar.php");
require_once("contenido/global+404.php");
require_once("contenido/admin+reportes.php");
/*-----------------------INICIALIZACIÓN-------------------*/
/* Hacer disponible la acción solicitada para todos*/
if ( isset($_GET[_ACC_]) ) { $_SESSION[_ACC_] = $_GET[_ACC_]; }
/* Hacer disponible a todos mi ubicación*/
$sURL_INDEX = $_SERVER['PHP_SELF'];
@date_default_timezone_set ('America/El_Salvador');
@ini_set("memory_limit","128M");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Content-Style-type" content="text/css" />
	<meta http-equiv="Content-Script-type" content="text/javascript" />
	<meta http-equiv="Content-Language" content="es" />
	<link rel="StyleSheet" href="estilo.css" type="text/css" />
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
	<link rel="start" href="/" />
	<title>Control de Laboratorio</title>
	<meta name="keywords" content="Laboratorio, UCA" />
	<meta name="description" content="Control de Laboratorio es un portal academico para gestionar laboratorios" />
	<!--[if IE]>
		<link rel="StyleSheet" href="/logo_msie.css" type="text/css" />
	<![endif]-->
</head>
<body>
	<div id="top"><h1>Sistema de Horarios</h1></div>
	<div id="container">
		<div id="leftwrapper">
			<?php MENU_usuario(); ?>
			<?php MENU_en_linea(); ?>
		</div>
	<div id="centerwrapper">
		<div id="content">
			<?php CONTENIDO_mostrar_principal(); ?>
		</div>
		<dl class="box"><dt><a href="javascript:history.go(-1)" title="Ir a la pantalla anterior">Regresar</a></dt></dl>
		<div class="clear"></div>
		</div>
	<div id="rightwrapper">
		<?php
		MENU_gestion();
		MENU_informacion();
		?>
	</div>
	<div class="clear"></div>
	</div>
	</div>
</body>
</html>
