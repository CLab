<?

 require_once("data.php");
define("DB_SERVER", $motor);
define("DB_USER", $usuario);
define("DB_PASS", $clave);
define("DB_NAME", $base);
define("TBL_USERS", "users");
define("TBL_ACTIVE_USERS",  "active_users");
define("TBL_ACTIVE_GUESTS", "active_guests");
define("TBL_BANNED_USERS",  "banned_users");
define("ADMIN_NAME", "admin");
define("GUEST_NAME", "Visitante");
define("ADMIN_LEVEL", 9);
define("USER_LEVEL",  1);
define("GUEST_LEVEL", 0);
define("TRACK_VISITORS", true);
define("USER_TIMEOUT", 10);
define("GUEST_TIMEOUT", 5);
define("COOKIE_EXPIRE", 60*60*24*100);
define("COOKIE_PATH", "/");
define("EMAIL_FROM_NAME", "Sistema de Horarios");
define("EMAIL_FROM_ADDR", "buho.uca@todosv.com");
define("EMAIL_WELCOME", true);
define("ALL_LOWERCASE", false);
?>
