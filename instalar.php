<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es" lang="es">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="Content-Style-type" content="text/css" />
	<meta http-equiv="Content-Script-type" content="text/javascript" />
	<meta http-equiv="Content-Language" content="es" />
	<link rel="StyleSheet" href="estilo.css" type="text/css" />
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
	<link rel="start" href="/" />
	<title>Instalador de Control de Laboratorio</title>
</head>
<body>
<?php
function CREAR_TBL($TBL,$QUERY) {
global $link;
$x = @mysql_query("DROP TABLE IF EXISTS $TBL;", $link) or die('!->No se pudo eliminar la tabla "'.$TBL.'".<br /><pre>' . mysql_error() . '</pre>');
$x = @mysql_query($QUERY, $link) or die('!->No se pudo crear la tabla "'. $TBL .'".<br /><pre>' . mysql_error() . '</pre>');
if ($x) {echo "- Creada: '$TBL'<br />";}
}

if (!isset($_POST['instalar'])) {
echo '
<h3>Sistema de Horarios - Instalador</h3><br />
<form action="'. $_SERVER['PHP_SELF'] .'" method="post">
<table border=0>
<tr><td>Configuración MySQL</td></tr>
<tr>
<td>Dirección del servidor MySQL:</td>
<td><input type="text" name="motor"  maxlength="50" size="20" value="localhost" /></td>
</tr>
<tr>
<td>Base de datos a utilizar:</td>
<td><input type="text" name="base"  maxlength="50" size="20" value="" /></td>
</tr>
<tr>
<td>Usuario:</td>
<td><input type="text" name="usuario"  maxlength="50" size="20" value="" /></td>
</tr>
<tr>
<td>Clave:</td>
<td><input type="password" name="clave"  maxlength="30" size="20" value="" /></td>
</tr>
<tr><td><br /><h2>Administración</h2></td></tr>
<tr>
<td>Nombre Administrador:</td>
<td><input type="text" name="admin"  maxlength="10" size="20" value="" /></td>
</tr>
<tr>
<td>Correo electrónico:</td>
<td><input type="text" name="email"  maxlength="50" size="20" value="" /></td>
</tr>
<tr>
<td>Clave:</td>
<td><input type="password" name="admin_clave"  maxlength="20" size="20" value="" /></td>
</tr>
<tr>
<td>Clave (repetir):</td>
<td><input type="password" name="admin_clave2"  maxlength="20" size="20" value="" /></td>
</tr>
</table>
<br />
<input type="submit" name="instalar" value="Instalar" />
</form>
';
} else {
echo '<b>cLab - Instalador : Instalando</b><br />';
if ($_POST['admin_clave'] != $_POST['admin_clave2']) {
echo '<h3>+Las contraseñas no coinciden.</h3><br />
<a href="javascript:history.back();">Regresar al instalador</a>';
}
echo '<h3>+Creando conexión a la base de datos...</h3><br />';
$link = @mysql_connect($_POST['motor'], $_POST['usuario'], $_POST['clave']) or die('Por favor revise sus datos, puesto que se produjo el siguiente error:<br /><pre>' . mysql_error() . '</pre>');
mysql_select_db($_POST['base'], $link) or die('!->La base de datos seleccionada "'.$_POST['base'].'" no existe');
echo '- Base de datos conectada...<br />';
echo '<h3>+Creando Archivo con datos de conexión...</h3><br />';
//touch("data.php");
//chmod("data.php", 0666);
$fh = @fopen("include/data.php", 'w') or die("No se pudo escribir 'data.php'.<br />");
if ($fh) {
$Datos = "<?php\n";
fwrite($fh, $Datos);
$Datos = '$motor = '. $_POST['motor'] .";\n" . '$usuario = '. $_POST['usuario'] .";\n". '$clave = '. $_POST['clave'] .";\n" . '$base = '. $_POST['base'] .";\n";
fwrite($fh, $Datos);
$Datos = "?>\n";
fwrite($fh, $Datos);
fclose($fh);
}
echo '- Creado<br />';
echo '<h3>+Creando Tablas...</h3><br />';
$q="CREATE TABLE users ( username varchar(10) primary key, password varchar(32), userid varchar(32), userlevel tinyint(1) unsigned not null, email varchar(50), timestamp int(11) unsigned not null, nombre varchar(100) not null, encargado varchar(100), catedratico varchar(100), tipo tinyint(1) unsigned not null, departamento tinyint(1) unsigned not null);";
CREAR_TBL("users", $q);
$q="CREATE TABLE active_users (username varchar(30) primary key, timestamp int(11) unsigned not null);";
CREAR_TBL("active_users", $q);
$q="CREATE TABLE active_guests (ip varchar(15) primary key, timestamp int(11) unsigned not null);";
CREAR_TBL("active_guests", $q);
$q="CREATE TABLE banned_users (username varchar(30) primary key, timestamp int(11) unsigned not null);";
CREAR_TBL("banned_users", $q);
$q="CREATE TABLE horarios (username varchar(10), taller varchar(5), dia tinyint(1), posicion int(11));";
CREAR_TBL("horarios", $q);
echo '<h3>+Creando usuario Admin...</h3><br />';
$q = "INSERT INTO users VALUES ('".$_POST['admin'] . "', '" . md5($_POST['admin_clave']) . "', '0', '9', '" . $_POST['email'] . "', " . time() . ",0,0,0,4,2)";
@mysql_query($q, $link);
echo '- Creado<br />';
mysql_close($link);
echo '<br /><b>Instalación completa</b><br />';
}
?>
</body>
</html>
